package com.example.asus_pc.distancecalculation.Api;

import com.example.asus_pc.distancecalculation.model.Weather;
import com.squareup.okhttp.ResponseBody;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServicesTutorial {

        @GET("api/directions/json?&sensor")
        Call<Response<okhttp3.ResponseBody>> getDistanceDuration(@Query("origin") String origin, @Query("destination") String destination, @Query("key") String key);

        String url = "https://maps.googleapis.com/maps/";
        public static final Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(url)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
}
