package com.example.asus_pc.distancecalculation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Polyline {
    @SerializedName("points")
    @Expose
    private String points;
}
