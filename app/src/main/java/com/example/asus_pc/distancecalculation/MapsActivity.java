package com.example.asus_pc.distancecalculation;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus_pc.distancecalculation.Api.ServicesTutorial;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        View.OnClickListener {

    private ServicesTutorial servicesTutorial;
    private GoogleMap mMap;
    private double longitude;
    private double latitude;
    private double fromLongitude;
    private double fromLatitude;
    private double toLongitude;
    private double toLatitude;
    private GoogleApiClient googleApiClient;
    private LinearLayout linearLayoutDistance;
    private TextView textViewDistance;
    private Button buttonSetTo;
    private Button buttonSetFrom;
    private Button buttonCalcDistance;
    private Button buttonClear;

    private Marker mMarkerA;
    private Marker mMarkerB;
    private Polyline mPolyline;
    private Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(AppIndex.API).build();

        linearLayoutDistance = findViewById(R.id.linearLayoutDistance);
        linearLayoutDistance.setVisibility(View.GONE);
        textViewDistance = findViewById(R.id.textViewDistance);
        buttonSetTo = findViewById(R.id.buttonSetTo);
        buttonSetFrom = findViewById(R.id.buttonSetFrom);
        buttonCalcDistance = findViewById(R.id.buttonCalcDistance);
        buttonClear = findViewById(R.id.buttonClear);

        buttonSetTo.setOnClickListener(this);
        buttonSetFrom.setOnClickListener(this);
        buttonCalcDistance.setOnClickListener(this);
        buttonClear.setOnClickListener(this);
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.asus_pc.distancecalculation/http/host/path")
        );
        AppIndex.AppIndexApi.start(googleApiClient, viewAction);
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.asus_pc.distancecalculation/http/host/path")
        );
        AppIndex.AppIndexApi.end(googleApiClient, viewAction);
    }

    private void getCurrentLocation() {
        mMap.clear();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            moveMap();
        }
    }

    private void moveMap() {
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .title("Current Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    public String makeURL (double sourcelat, double sourcelog, double destlat, double destlog ){
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString.append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString.append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        urlString.append("&key=AIzaSyA64zZk-57rzKHWevwLUMtcQQNJXv-Tq-s");
        return urlString.toString();
    }

//    private void getDirection(){
//        String url = makeURL(fromLatitude, fromLongitude, toLatitude, toLongitude);
//        final ProgressDialog loading = ProgressDialog.show(this, "Getting Route", "Please wait...", false, false);
//
//        StringRequest stringRequest = new StringRequest(url,
//                new com.android.volley.Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        loading.dismiss();
//                        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAA " + response);
//                        drawPath(response);
//                    }
//                },
//                new com.android.volley.Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        loading.dismiss();
//                    }
//                });
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
//    }

//    public void drawPath(String  result) {
//        LatLng from = new LatLng(fromLatitude,fromLongitude);
//        LatLng to = new LatLng(toLatitude,toLongitude);
//        Double distance = SphericalUtil.computeDistanceBetween(from, to);
//        Toast.makeText(this,String.valueOf(distance+" Meters"),Toast.LENGTH_SHORT).show();
//
//        try {
//            final JSONObject json = new JSONObject(result);
//            System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL " + gson.toJson(json));
//            JSONArray routeArray = json.getJSONArray("routes");
//            JSONObject routes = routeArray.getJSONObject(0);
//            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
//            String encodedString = overviewPolylines.getString("points");
//            System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL " + encodedString);
//            List<LatLng> list = decodePoly(encodedString);
//            Polyline line = mMap.addPolyline(new PolylineOptions()
//                    .addAll(list)
//                    .width(20)
//                    .color(Color.RED)
//                    .geodesic(true)
//            );
//        }
//        catch (JSONException e) {
//
//        }
//    }

//    public void drawPath(String result) {
//        String distanceInKm;
//        //Getting both the coordinates
//        LatLng from = new LatLng(fromLatitude,fromLongitude);
//        LatLng to = new LatLng(toLatitude,toLongitude);
//        Double distance = SphericalUtil.computeDistanceBetween(from, to);
//
////        mMap.addPolyline(new PolylineOptions()
////                .add(new LatLng(fromLatitude, fromLongitude), new LatLng(toLatitude, toLongitude))
////                .width(5)
////                .color(Color.RED))
////                .setGeodesic(true);
//
//        DecimalFormat df = new DecimalFormat("#.#");
//        distanceInKm = (df.format(distance / 1000));
//        linearLayoutDistance.setVisibility(View.VISIBLE);
//        textViewDistance.setText(distanceInKm + " km");
//
//        try {
//            final JSONObject json = new JSONObject(result);
//            JSONArray routeArray = json.getJSONArray("routes");
//            JSONObject routes = routeArray.getJSONObject(0);
//            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
//            System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA " + overviewPolylines);
//            String encodedString = overviewPolylines.getString("points");
//            List<LatLng> list = decodePoly(encodedString);
//            System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA " + list);
//
//            PolylineOptions options = new PolylineOptions()
//                    .width(5)
//                    .color(Color.BLUE)
//                    .geodesic(true);
//
//            for (int z = 0; z < list.size(); z++) {
//                LatLng point = list.get(z);
//                options.add(point);
//            }
//            mMap.addPolyline(options);
//        }catch (JSONException e) {
//
//        }
//    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng( (((double) lat / 1E5)),
                    (((double) lng / 1E5) ));
            poly.add(p);
        }
        return poly;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                checkLocationPermission();
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

//        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
//            @Override
//            public void onMyLocationChange(Location location) {
//                CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 18);
//                mMap.animateCamera(cu);
//            }
//        });

//        LatLng latLng = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION );
                            }
                        })
                        .create()
                        .show();


            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION );
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        linearLayoutDistance.setVisibility(View.GONE);
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true));

        latitude = latLng.latitude;
        longitude = latLng.longitude;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;
        moveMap();
    }

    @Override
    public void onClick(View v) {
        if(v == buttonSetFrom){
            linearLayoutDistance.setVisibility(View.GONE);
            fromLatitude = latitude;
            fromLongitude = longitude;
            buttonSetFrom.setBackgroundColor(Color.GREEN);
            Toast.makeText(this,"From set",Toast.LENGTH_SHORT).show();
        }

        if(v == buttonSetTo){
            linearLayoutDistance.setVisibility(View.GONE);
            toLatitude = latitude;
            toLongitude = longitude;
            buttonSetTo.setBackgroundColor(Color.GREEN);
            Toast.makeText(this,"To set",Toast.LENGTH_SHORT).show();
        }

        if(v == buttonCalcDistance){
            buttonCalcDistance.setBackgroundColor(Color.GREEN);
            System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD " + fromLatitude + "; " + fromLongitude + "; " + toLatitude + "; " + toLongitude);
//            getDirection();
            getDirectionInRetrofit(fromLatitude, fromLongitude, toLatitude, toLongitude);
        }

        if(v == buttonClear){
            buttonSetFrom.setBackgroundResource(android.R.drawable.btn_default);
            buttonSetTo.setBackgroundResource(android.R.drawable.btn_default);
            buttonCalcDistance.setBackgroundResource(android.R.drawable.btn_default);
            mMap.clear();
            linearLayoutDistance.setVisibility(View.GONE);
        }
    }

//    private void showDistance() {
//        double distance = SphericalUtil.computeDistanceBetween(mMarkerA.getPosition(), mMarkerB.getPosition());
//        textViewDistance.setText("The markers are " + formatNumber(distance) + " apart.");
//    }
//
//    private void updatePolyline() {
//        mPolyline.setPoints(Arrays.asList(mMarkerA.getPosition(), mMarkerB.getPosition()));
//    }
//
//    private String formatNumber(double distance) {
//        String unit = "m";
//        if (distance < 1) {
//            distance *= 1000;
//            unit = "mm";
//        } else if (distance > 1000) {
//            distance /= 1000;
//            unit = "km";
//        }
//
//        return String.format("%4.3f%s", distance, unit);
//    }

    private void getDirectionInRetrofit(double sourcelat, double sourcelog, double destlat, double destlog){
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAA");
        servicesTutorial = ServicesTutorial.retrofit.create(ServicesTutorial.class);
        Call<Response<okhttp3.ResponseBody>> call = servicesTutorial.getDistanceDuration(sourcelat + "," + sourcelog,destlat + "," + destlog, getResources().getString(R.string.google_maps_key));
        call.enqueue(new Callback<Response<okhttp3.ResponseBody>>() {
            @Override
            public void onResponse(Call<Response<okhttp3.ResponseBody>> call, Response<Response<okhttp3.ResponseBody>> response) {
                System.out.println("EEEEEEEEEEEEEEEEEEEEEEEEEE " + response.body().);
                try {
//                    String distance = response.body().getLegs().get(0).getDistance().getText();
//                    String time = response.body().getLegs().get(0).getDuration().getText();
//                    textViewDistance.setText("Distance:" + distance + ", Duration:" + time);
//                    String encodedString = response.body().getOverviewPolyline().getPoints();
//                    List<LatLng> list = decodePoly(encodedString);
//                    mMap.addPolyline(new PolylineOptions()
//                            .addAll(list)
//                            .width(20)
//                            .color(Color.RED)
//                            .geodesic(true)
//                    );
                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Response<okhttp3.ResponseBody>> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }

}